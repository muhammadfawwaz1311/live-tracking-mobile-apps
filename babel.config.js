module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: [
          { 'atoms/*': './src/components/atoms/*' },
          { 'icons/*': './src/assets/icons/*' },
          { 'images/*': './src/assets/images/*' },
          { 'molecules/*': './src/components/molecules/*' },
          { 'navigation/*': './src/navigation/*' },
          { 'screens/*': './src/screens/*' },
          { 'store/*': './src/store/*' },
          { 'templates/*': './src/components/templates/*' },
          { 'utils/*': './src/utils/*' }
        ],
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    ]
  ]
}
