import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MenuNav } from 'navigation'
import { Orders, Promos, Wishlist } from 'screens'
import { getIcon } from 'utils'

const HomeStack = createBottomTabNavigator()

const HomeNav = (): JSX.Element => {
  return (
    <HomeStack.Navigator
      initialRouteName="MenuNav"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, focused, size }) => {
          const routeName: string = route.name
          return getIcon({ color, focused, routeName, size })
        },
        tabBarLabelStyle: { fontSize: 16, color: '#000000' },
        tabBarActiveTintColor: '#9D44C0',
        tabBarInactiveTintColor: '#000000',
        headerShown: false,
        tabBarHideOnKeyboard: true
      })}>
      <HomeStack.Screen
        name="MenuNav"
        component={MenuNav}
        options={{ title: 'Menu' }}
      />
      <HomeStack.Screen name="Promos" component={Promos} />
      <HomeStack.Screen name="Wishlist" component={Wishlist} />
      <HomeStack.Screen name="Orders" component={Orders} />
    </HomeStack.Navigator>
  )
}

export default HomeNav
