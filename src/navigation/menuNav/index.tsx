import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Cart, Menu, Messages, Notifications, Profile } from 'screens'

const MenuStack = createNativeStackNavigator()

const MenuNav = (): JSX.Element => {
  return (
    <MenuStack.Navigator initialRouteName="Menu">
      <MenuStack.Screen
        name="Menu"
        component={Menu}
        options={{ headerShown: false }}
      />
      <MenuStack.Screen name="Messages" component={Messages} />
      <MenuStack.Screen name="Notifications" component={Notifications} />
      <MenuStack.Screen name="Cart" component={Cart} />
      <MenuStack.Screen name="Profile" component={Profile} />
    </MenuStack.Navigator>
  )
}

export default MenuNav
