import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Splash } from 'components/templates'
import HomeNav from './homeNav'

const RootStack = createNativeStackNavigator()

const Router = (): JSX.Element => {
  return (
    <RootStack.Navigator
      initialRouteName="Splash"
      screenOptions={{ headerShown: false }}>
      <RootStack.Screen name="Splash" component={Splash} />
      <RootStack.Screen name="HomeNav" component={HomeNav} />
    </RootStack.Navigator>
  )
}

export default Router
