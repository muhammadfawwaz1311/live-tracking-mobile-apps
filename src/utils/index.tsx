import React from 'react'
import IconAD from 'react-native-vector-icons/AntDesign'
import IconIO from 'react-native-vector-icons/Ionicons'
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons'

interface IconProps {
  color: string
  focused: boolean
  routeName: string
  size: number
}

export const getIcon = ({
  color,
  focused,
  routeName,
  size
}: IconProps): JSX.Element => {
  const type = focused ? 'focused' : 'normal'
  const icon = {
    MenuNav: {
      focused: <IconIO name="restaurant" size={size} color={color} />,
      normal: <IconIO name="restaurant-outline" size={size} color={color} />
    },
    Orders: {
      focused: <IconMC name="clipboard-clock" size={size} color={color} />,
      normal: (
        <IconMC name="clipboard-clock-outline" size={size} color={color} />
      )
    },
    Promos: {
      focused: <IconMC name="ticket-percent" size={size} color={color} />,
      normal: <IconMC name="ticket-percent-outline" size={size} color={color} />
    },
    Wishlist: {
      focused: <IconAD name="heart" size={size} color={color} />,
      normal: <IconAD name="hearto" size={size} color={color} />
    }
  }

  return icon[routeName as keyof typeof icon][type]
}
