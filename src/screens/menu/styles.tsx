import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: { flex: 1 },
  content: { flex: 1, padding: 10, backgroundColor: '#FFFFFF' },
  header: {
    alignItems: 'center',
    height: 70,
    display: 'flex',
    flexDirection: 'row',
    gap: 15,
    paddingHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: '#FFFFFF'
  },
  iconButton: {
    width: 30,
    color: '#060308'
  },
  profile: {
    width: 30,
    height: 30,
    borderRadius: 50,
    borderColor: '#9D44C0',
    borderWidth: 2,
    objectFit: 'cover'
  },
  search: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#060308',
    fontSize: 16
  }
})
