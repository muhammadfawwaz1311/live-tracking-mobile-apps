import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './styles'

const Messages = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text>Messages</Text>
    </View>
  )
}

export default Messages
