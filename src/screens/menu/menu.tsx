import React from 'react'
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import IconIO from 'react-native-vector-icons/Ionicons'
import { ImageAssets } from 'assets/images'
import { styles } from './styles'

const Menu = ({ navigation }: any): JSX.Element => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <View style={{ flex: 1 }}>
          <TextInput style={styles.search} placeholder="Search" />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Messages')}>
          <IconIO name="mail-outline" size={30} style={styles.iconButton} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Notifications')}>
          <IconIO
            name="notifications-outline"
            size={30}
            style={styles.iconButton}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
          <IconIO name="cart-outline" size={30} style={styles.iconButton} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
          <Image source={ImageAssets.profile} style={styles.profile} />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.content}>
        <Text>Menu</Text>
      </ScrollView>
    </SafeAreaView>
  )
}

export default Menu
