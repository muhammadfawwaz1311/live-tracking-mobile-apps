import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './styles'

const Cart = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text>Cart</Text>
    </View>
  )
}

export default Cart
