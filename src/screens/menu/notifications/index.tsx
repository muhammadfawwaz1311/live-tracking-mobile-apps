import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './styles'

const Notifications = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text>Notifications</Text>
    </View>
  )
}

export default Notifications
