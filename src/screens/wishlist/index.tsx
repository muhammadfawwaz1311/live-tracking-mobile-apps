import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './styles'

const Wishlist = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text>Wishlist</Text>
    </View>
  )
}

export default Wishlist
