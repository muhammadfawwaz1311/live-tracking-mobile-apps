import React from 'react'
import { View, Text } from 'react-native'
import { styles } from './styles'

const Orders = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text>Orders</Text>
    </View>
  )
}

export default Orders
