const ImageAssets = {
  foodTruck: require('./food-truck.png'),
  profile: require('./profile-picture.png')
}

export { ImageAssets }
