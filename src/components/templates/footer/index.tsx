import React from 'react'
import { View, Text } from 'react-native'
import { styles } from './styles'

const Footer = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Footer</Text>
    </View>
  )
}

export default Footer
