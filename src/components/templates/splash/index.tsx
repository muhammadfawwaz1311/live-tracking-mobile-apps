import React, { useEffect } from 'react'
import { Image, Text, View } from 'react-native'
import { styles } from './styles'
import { ImageAssets } from 'assets/images'

const Splash = ({ navigation }: any): JSX.Element => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('HomeNav')
    }, 2000)
  }, [])

  return (
    <View style={styles.container}>
      <Image
        source={ImageAssets.foodTruck}
        style={{ width: 400, height: 400 }}
      />
      <View style={styles.textWrapper}>
        <Text style={styles.pre}>from</Text>
        <Text style={styles.desc}>Papaw</Text>
      </View>
    </View>
  )
}

export default Splash
