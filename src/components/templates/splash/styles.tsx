import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: 'transparent'
  },
  textWrapper: { alignItems: 'center', justifyContent: 'center' },
  pre: {
    color: '#9D44C0',
    fontSize: 18
  },
  desc: {
    color: '#9D44C0',
    fontSize: 20,
    fontWeight: 'bold'
  }
})
