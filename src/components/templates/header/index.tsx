import React from 'react'
import { Text, View } from 'react-native'
import { styles } from './styles'

const Header = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Home</Text>
      </View>
    </View>
  )
}

export default Header
