import Footer from './footer'
import Header from './header'
import Splash from './splash'

export { Footer, Header, Splash }
